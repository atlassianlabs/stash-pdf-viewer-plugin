package com.atlassian.stash.plugin;

import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.soy.renderer.SoyException;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.atlassian.stash.content.ContentService;
import com.atlassian.stash.exception.AuthorisationException;
import com.atlassian.stash.history.HistoryService;
import com.atlassian.stash.i18n.I18nService;
import com.atlassian.stash.io.TypeAwareOutputSupplier;
import com.atlassian.stash.repository.Repository;
import com.atlassian.stash.repository.RepositoryMetadataService;
import com.atlassian.stash.repository.RepositoryService;
import com.atlassian.stash.user.StashAuthenticationContext;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import javax.annotation.Nonnull;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URI;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static javax.servlet.http.HttpServletResponse.SC_BAD_REQUEST;
import static org.apache.commons.lang.StringUtils.isBlank;

public class RenderPdfServlet extends HttpServlet {
    private static final Logger log = LoggerFactory.getLogger(RenderPdfServlet.class);

    private static final Pattern PATH_RX = Pattern.compile("^/projects/([^/]+)/repos/([^/]+)/(.*)$");

    private final RepositoryService repositoryService;
    private final I18nService i18nService;
    private final StashAuthenticationContext authenticationContext;
    private final LoginUriProvider loginUriProvider;
    private final RepositoryMetadataService repositoryMetadataService;
    private final HistoryService historyService;
    private final WebResourceManager webResourceManager;
    private final SoyTemplateRenderer soyTemplateRenderer;
    private final ContentService contentService;

    public RenderPdfServlet(RepositoryService repositoryService, StashAuthenticationContext authenticationContext,
                         I18nService i18nService, WebResourceManager webResourceManager,
                         SoyTemplateRenderer soyTemplateRenderer,
                         RepositoryMetadataService repositoryMetadataService,
                         HistoryService historyService,
                         ContentService contentService,
                         LoginUriProvider loginUriProvider) {
        this.repositoryService = repositoryService;
        this.i18nService = i18nService;
        this.authenticationContext = authenticationContext;
        this.loginUriProvider = loginUriProvider;
        this.repositoryMetadataService = repositoryMetadataService;
        this.historyService = historyService;
        this.webResourceManager = webResourceManager;
        this.soyTemplateRenderer = soyTemplateRenderer;
        this.contentService = contentService;
    }

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        String raw = req.getParameter("raw");
        final PathAtCommit pathAtCommit = resolvePathAtCommit(req, resp);
        if (pathAtCommit == null) {
            return;
        }

        final Map<String, Object> context = createContext(pathAtCommit);
        String content = resolveContent(pathAtCommit, context);

        if (!isBlank(raw)) {
            renderRawPdf(resp, content);
            return;
        }

        context.put("content", content);
        render(resp, "stash.pdfviewer.page", context);
    }

    private void renderRawPdf(HttpServletResponse resp, String content) throws IOException {
        resp.setContentType("application/pdf;charset=UTF-8");
        resp.getOutputStream().write(content.getBytes());
        resp.getOutputStream().close();
    }

    private String resolveContent(PathAtCommit pathAtCommit, final Map<String, Object> context) {
        final ByteArrayOutputStream contentBuffer = new ByteArrayOutputStream();
        contentService.streamFile(pathAtCommit.getRepository(), pathAtCommit.getCommit(), pathAtCommit.getPath(), new TypeAwareOutputSupplier() {
            @Override
            public OutputStream getStream(@Nonnull String contentType) throws IOException {
                context.put("contentType", contentType);
                return contentBuffer;
            }
        });
        return contentBuffer.toString();
    }

    private Map<String, Object> createContext(PathAtCommit pathAtCommit) {
        final Map<String, Object> context = Maps.newHashMap();
        context.put("repository", pathAtCommit.getRepository());
        context.put("path", pathAtCommit.getPath());
        context.put("at", pathAtCommit.getCommit());
        return context;
    }

    private PathAtCommit resolvePathAtCommit(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        // Resolve the repository from the servlet path, flushing a friendly error message to the user if there are any
        // problems parsing the URI or if the repository is missing.
        Matcher m = PATH_RX.matcher(req.getPathInfo());
        if (!m.find()) {
            renderErrorMessageNoContext(resp, i18nService.getText("stash.repo.servlet.bad.path",
                    "Bad path, must match ''{0}''.", PATH_RX.pattern()));
            return null;
        }

        Repository repository = null;
        try {
            repository = repositoryService.findBySlug(m.group(1), m.group(2));
        } catch (AuthorisationException e) {
            // ignore - we handle a missing/no_authz repository below in the same manner to not leak repository names
        }

        if (repoOrUserDontExist(req, resp, repository)) return null;

        // resolve path
        String path = m.group(3);
        if (isBlank(path)) {
            resp.sendError(SC_BAD_REQUEST, "A repository path is required.");
            return null;
        }
        path = path.trim();

        return new PathAtCommit(repository, path, resolveCommit(req, repository));
    }

    private String resolveCommit(HttpServletRequest req, Repository repository) {
        String at = req.getParameter("at");
        if (isBlank(at)) {
            // use default branch if unspecified
            at = repositoryMetadataService.getDefaultBranch(repository).getLatestChangeset();
        } else {
            // otherwise make sure we have the actual commit SHA, not just a ref
            at = historyService.getChangeset(repository, at).getId();
        }
        return at;
    }

    private boolean repoOrUserDontExist(HttpServletRequest req, HttpServletResponse resp, Repository repository) throws IOException, ServletException {
        if (repository == null) {
            // Couldn't resolve the repository.. check if this is because the user isn't logged in (Stash didn't
            // support anonymous access at time of writing) or because the context user doesn't have the REPO_READ
            // permission.
            if (authenticationContext.getCurrentUser() == null) {
                redirectToLoginPage(req, resp);
                return true;
            } else {
                // render authz error message
                renderErrorMessageNoContext(resp, i18nService.getText("stash.repo.servlet.no.such.repository",
                        "The specified repository does not exist or you have insufficient permissions to access it."));
                return true;
            }
        }
        return false;
    }

    private void redirectToLoginPage(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        // redirect to login
        StringBuffer originalUrl = req.getRequestURL();
        if (!isBlank(req.getQueryString())) {
            originalUrl.append("?").append(req.getQueryString());
        }
        resp.sendRedirect(loginUriProvider.getLoginUri(URI.create(originalUrl.toString())).toASCIIString());
    }

    private void render(HttpServletResponse resp, String templateName, Map<String, Object> data) throws IOException, ServletException {
        resp.setContentType("text/html;charset=UTF-8");
        try {
            webResourceManager.requireResourcesForContext("plugin.page.pdf-viewer");
            soyTemplateRenderer.render(resp.getWriter(),
                    "com.atlassian.stash.plugin.stash-pdf-viewer-plugin:pdf-viewer-server-side-soy", templateName, data);
        } catch (SoyException e) {
            Throwable cause = e.getCause();
            if (cause instanceof IOException) {
                throw (IOException) cause;
            }
            throw new ServletException(e);
        }
    }

    private void renderErrorMessageNoContext(HttpServletResponse resp, String errorMessage) throws IOException, ServletException {
        render(resp, "stash.pdfviewer.notAvailable", ImmutableMap.<String, Object>of("errorMessage", errorMessage));
    }
}
