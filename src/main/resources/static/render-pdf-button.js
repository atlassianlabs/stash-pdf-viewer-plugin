define('plugin/render-pdf-button',
    ['exports', 'jquery', 'underscore', 'aui', 'eve'],
    function(exports, $, _, AJS, events) {

        function getParameterByName(name){
            name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
            var regexS = "[\\?&]" + name + "=([^&#]*)";
            var regex = new RegExp(regexS);
            var results = regex.exec(window.location.search);
            if(results == null)
                return "";
            else
                return decodeURIComponent(results[1].replace(/\+/g, " "));
        }

        function currentPath() {
            var url = location.pathname.match(/\/projects\/(.*)\/repos\/(.*)\/browse\/(.*)(?:$|\?)/);
            var at = getParameterByName("at");
            if (!at) {
                at = "HEAD";
            }

            return {
                pKey: url[1],
                repo: url[2],
                path: url[3],
                at: at
            };
        }

        function encodePathSegments(path) {
            return _.map(path.split("/"), function (component) {
                return encodeURIComponent(component);
            }).join('/');
        }

        function endsWith(suffix, str) {
            return str.indexOf(suffix, str.length - suffix.length) !== -1;
        }

        exports.addRenderPdfButton = function(parentSelector) {
            var $renderPdfButton = $("<a class='aui-button' id='render-pdf-button'>Render PDF</a>");

            var appendButton = function() {
                var path = currentPath();
                if (endsWith('.pdf', path.path) &&  $('#render-pdf-button').length === 0) {
                    $(parentSelector).append(
                        $renderPdfButton
                            .clone()
                            .click(function() {
                                location.href = AJS.contextPath() + "/plugins/servlet/render-pdf/projects/" +
                                    encodeURIComponent(path.pKey) + "/repos/" + encodeURIComponent(path.repo) +
                                    "/" + encodePathSegments(path.path) + "?at=" + encodeURIComponent(path.at);
                            })
                    );
                }
            };

            // no client-side web-items.. re-add the button whenever the source view is updated
            events.on("stash.feature.sourceview.dataLoaded", appendButton);
            events.on("stash.feature.sourceview.onBinary", appendButton);
        }

    }
);

AJS.$(document).ready(function() {
    require('plugin/render-pdf-button').addRenderPdfButton(".source-commands .aui-toolbar2-primary");
});