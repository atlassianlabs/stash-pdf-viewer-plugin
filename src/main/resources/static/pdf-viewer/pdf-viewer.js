define('plugin/pdf-viewer',
    ['exports', 'jquery', 'underscore', 'aui', 'util/navbuilder', 'model/page-state'],
    function(exports, $, _, AJS, navBuilder, pageState) {

    var pdfDoc = null,
        pageNum = 1,
        canvas = document.getElementById('the-canvas'),
        ctx = canvas.getContext('2d');

    //
    // Get page info from document, resize canvas accordingly, and render page
    //
    exports.renderPage = function(pdfDoc, num, width, height) {
        // Using promise to fetch the page
        pdfDoc.getPage(num).then(function(page) {
            exports.page = page;
            var viewport = page.getViewport(exports.scale);
            if (width == null && height == null) {
                canvas.height = viewport.height;
                canvas.width = viewport.width;
            } else {
                canvas.height = height;
                canvas.width = width;
            }

            // Render PDF page into canvas context
            var renderContext = {
                canvasContext: ctx,
                viewport: viewport
            };
            page.render(renderContext);
        });

        // Update page counters
        document.getElementById('page_num').textContent = pageNum;
        document.getElementById('page_count').textContent = pdfDoc.numPages;
    }

    //
    // Go to previous page
    //
    exports.refresh = function() {
        console.log('scale is:' + exports.scale);
        console.log('fullscreen=' + document.webkitIsFullScreen);
//        if (document.webkitIsFullScreen) {
//            exports.scale = 1;
//        } else {
//            exports.scale = 0.5;
//        }
//        exports.scale = 1;
        exports.renderPage(pdfDoc, pageNum, null, null);
    }
    //
    // Go to previous page
    //
    exports.goPrevious = function() {
        console.log('fullscreen=' + document.webkitIsFullScreen);
        if (document.webkitIsFullScreen) {
            exports.scale = 1;
        } else {
            exports.scale = 0.5;
        }
        if (pageNum <= 1)
            return;
        pageNum--;
        console.log('scale is:' + exports.scale);
        exports.renderPage(pdfDoc, pageNum, null, null);
    }

    //
    // Go to next page
    //
    exports.goNext = function() {
        console.log('fullscreen=' + document.webkitIsFullScreen);
        if (document.webkitIsFullScreen) {
            exports.scale = 1;
        } else {
            exports.scale = 0.5;
        }
        if (pageNum >= pdfDoc.numPages)
            return;
        pageNum++;
        console.log('scale is:' + exports.scale);
        exports.renderPage(pdfDoc, pageNum, null, null);
    }

    exports.launchFullScreen = function(element) {
            if(element.requestFullScreen) {
                element.requestFullScreen();
            } else if(element.mozRequestFullScreen) {
                element.mozRequestFullScreen();
            } else if(element.webkitRequestFullScreen) {
                element.webkitRequestFullScreen();
            }
            exports.renderPage(pdfDoc, pageNum, 1024, 768);
    }

    exports.onReady = function() {
        PDFJS.disableWorker = true;
        //
        // Asynchronously download PDF as an ArrayBuffer
        //
        PDFJS.getDocument(window.location + "&raw=true").then(function getPdfHelloWorld(_pdfDoc) {
            pdfDoc = _pdfDoc;
            exports.pdfDoc = pdfDoc;
            exports.renderPage(pdfDoc, pageNum);
        });
    };
});

jQuery(document).ready(function () {
    var $ = jQuery,
        $the_canvas = $('#the-canvas'),
        pdfViewer = require('plugin/pdf-viewer');
        pdfViewer.scale = 0.5;

    $(document).keyup(function(event) {
        if ( event.which == 39 // right arrow
            || event.which == 40 // down arrow
            || event.which == 34 // page down
            || event.which == 32 // space
            ) {
            event.preventDefault();
            pdfViewer.goNext();
        }
        if ( event.which == 37 // left arrow
            || event.which == 38 // up arrow
            || event.which == 33 // page up
            ) {
            event.preventDefault();
            pdfViewer.goPrevious();
        }
    });

    document.addEventListener("webkitfullscreenchange", function () {
        if (!document.webkitIsFullScreen) {
            pdfViewer.scale = 0.5;
            pdfViewer.refresh();
        }
    }, false);

    $("#fullscreen-btn").click(function () {
        pdfViewer.launchFullScreen($the_canvas.get(0));
        setTimeout(function(){
            pdfViewer.scale = 1;
            pdfViewer.refresh();
        },0);
    })
});