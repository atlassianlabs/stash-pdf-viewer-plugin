# PDF.js plugin to display embedded PDF content in Stash

This plugin adds a "Render PDF" button to the file view of Stash.
When the button is clicked the contents of the PDF are displayed in line using
PDF.js which lives at http://mozilla.github.io/pdf.js/
